const classDarkTheme = 'dark-theme';
const classIconTheme = 'bx-sun';
const classSelectedTheme = 'selected-theme';
const classSelectedIcon = 'selected-icon';

/**
 * Get the current theme.
 * 
 * @returns {string}
 */
export const getCurrentTheme = () => {
    return document.body.classList.contains(classDarkTheme) ? 'dark' : 'light';
};

/**
 * Get the current theme icon.
 * 
 * @param   {HTMLElement} themeButton Toggle theme button
 * @returns {string}
 */
export const getCurrentIcon = (themeButton) => {
    return themeButton.classList.contains(classIconTheme) ? 'bx bx-moon' : 'bx bx-sun';
};

/**
 * Toggle previous theme the user has selected and
 * stored in localStorage.
 * 
 * @param {string} previousThemeSelected Previous theme selected in localStorage
 */
export const toggleThemeBasedOnPreviousChoice = (previousThemeSelected) => {
    document.body.classList[previousThemeSelected === 'dark' ? 'add' : 'remove'](classDarkTheme);
};

/**
 * 
 * @param {HTMLElement} themeButton          Toggle theme button
 * @param {string}      previousIconSelected Previous theme icon in localStorage
 */
export const toggleIconThemeBasedOnPreviousChoice = (themeButton, previousIconSelected) => {
    themeButton.classList[previousIconSelected === 'bx bx-moon' ? 'add' : 'remove'](classIconTheme);
};

/**
 * Get the previous theme from localStorage.
 * 
 * @returns {string}
 */
export const getPreviousTheme = () => {
    return localStorage.getItem(classSelectedTheme);
};

/**
 * Get the previous theme icon from localStorage.
 * 
 * @returns {string}
 */
export const getPreviousIcon = () => {
    return localStorage.getItem(classSelectedIcon);
};

/**
 * Toggle the dark/light theme when user clicks on
 * the button.
 * 
 * @param {HTMLElement} themeButton Toggle theme button
 */
export const toggleTheme = (themeButton) => {

    themeButton.addEventListener('click', () => {

        document.body.classList.toggle(classDarkTheme)
        themeButton.classList.toggle(classIconTheme)

        localStorage.setItem(classSelectedTheme, getCurrentTheme())
        localStorage.setItem(classSelectedIcon, getCurrentIcon(themeButton))
    });

};