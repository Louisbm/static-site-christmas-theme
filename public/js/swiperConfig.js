/**
 * Configure the swiper library on HTML
 * elements.
 */
export const configSwiper = () => {

    new Swiper(".new-swiper", {
        spaceBetween: 24,
        loop: 'true',
        slidesPerView: "auto",
        centeredSlides: true,
        
        pagination: {
          el: ".swiper-pagination",
          dynamicBullets: true,
        },
        breakpoints: {
            992: {
              spaceBetween: 80,
            },
        },
    });

};