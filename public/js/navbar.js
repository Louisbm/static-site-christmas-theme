const classShowMenu = 'show-menu';
const classScrollHeader = 'scroll-header';


/**
 * Open the navbar when clicking on the
 * toggle button.
 * 
 * @param {HTMLElement} navToggle Toggle button
 * @param {HTMLElement} navMenu   Navbar menu to toggle
 */
export const openNavbar = (navToggle, navMenu) => {

    if (!navToggle) return;

    navToggle.addEventListener('click', () => {
        navMenu.classList.add(classShowMenu);
    });

};

/**
 * Close the navbar when clicking on the
 * close button.
 * 
 * @param {HTMLElement} navClose Close button
 * @param {HTMLElement} navMenu  Navbar menu to close
 */
export const closeNavbar = (navClose, navMenu) => {

    if (!navClose) return;

    navClose.addEventListener('click', () => {
        navMenu.classList.remove(classShowMenu);
    });

};

/**
 * Close the navbar when clicking on a link
 * inside (mobile version).
 * 
 * @param {HTMLElement} linkElement Link element inside navbar
 * @param {HTMLElement} navMenu     Navbar menu to close
 */
export const closeNavbarOnLinkClicked = (linkElement, navMenu) => {

    linkElement.addEventListener('click', () => {
        navMenu.classList.remove(classShowMenu);
    });

};

/**
 * Add/remove sticky navbar when user
 * scroll.
 * 
 * @param {HTMLElement} headerElement Header
 */
export const toggleStickyHeader = (headerElement) => {

    if (window.scrollY >= 50) {
        headerElement.classList.add(classScrollHeader);
        return;
    }

    headerElement.classList.remove(classScrollHeader);
    return;

};

/**
 * Highlight the corresponding link inside the navbar.
 * 
 * @param {NodeList} sectionsList List of sections
 */
export const highlightActiveLink = (sectionsList) => {

    const scrollY = window.pageYOffset

    sectionsList.forEach((currentSection) => {

        const sectionHeight = currentSection.offsetHeight;
        const sectionTop = currentSection.offsetTop - 58;
        const sectionId = currentSection.getAttribute('id');

        if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.add('active-link');
            return;
        }

        document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.remove('active-link');
        return;
    });

};