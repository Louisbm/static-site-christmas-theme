/**
 * Toggle the scroll up button.
 * 
 * @param {HTMLElement} scrollUpButton 
 */
export const toggleScrollUpButton = (scrollUpButton) => {

    if (window.scrollY >= 350) {
        scrollUpButton.classList.add('show-scroll');
        return;
    }

    scrollUpButton.classList.remove('show-scroll');
    return;

};