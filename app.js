const express = require('express');

const app = express();

app.set('views', `${__dirname}/views`);
app.set('view engine', 'ejs');

app.use(express.static(`${__dirname}/public`));

app.get('/', (req, res) => {

    res.status(200).render('landing', {
        title: 'Noël'
    });

});

const port = 3000;

app.listen(port, () => {
    console.log(`Server listen on port ${port}`);
});
